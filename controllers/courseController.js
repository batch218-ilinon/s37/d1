const mongoose = require("mongoose")
const Course = require("../models/course.js");

module.exports.addCourse = (reqBody) => {
	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description, 
		price: reqBody.price
	})
	return newCourse.save().then((newCourse, error) => {
		if(error){
			return error;
		}
		else{
			return newCourse;
		}
	})
}

module.exports.getAllCourse = () => {
	return Course.find({}).then(result => {return result})
}

module.exports.getActiveCourses = () => {
	return Course.find({isActive:true}).then(result => {
		return result
	})
}

module.exports.getCourse = (courseId) => {
	return Course.findById(courseId).then(result => {
		return result;
	})
}

module.exports.updateCourse = (courseId, newData) => {
	if(newData.isAdmin == true){
		return Course.findByIdAndUpdate(courseId, 
			{
				name: newData.course.name,
				description: newData.course.description,
				price: newData.course.price	
			}
		).then((updatedCourse, error) => {
			if(error){
				return false
			}
			return true
		})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to access this');
		return message.then((value) => {return value})
	}
}

// Part 3
module.exports.createCourse = (newData) => {
	console.log(newData.isAdmin)
	if(newData.isAdmin == true){
		let newCourse = new Course({
			name: newData.course.name,
			description: newData.course.description,
			price: newData.course.price
		})
		return newCourse.save().then((newCourse, error) => {
			if(error){
				return error;
			}
			else{
				return newCourse;
			}
		})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to access this');
		return message.then((value) => {return value})
	}
}

// Part 4
module.exports.archiveCourse = (courseId, newData) => {
	if(newData.isAdmin == true){
		return Course.findByIdAndUpdate(courseId, 
			{
				isActive: newData.course.isActive
			}
		).then((archiveCourse, error) => {
			if(error){
				return false
			}
			return true
		})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to access this');
		return message.then((value) => {return value})
	}
}

module.exports.enroll = async(data) => {
	let isUserUpdated = await User.findById(data.userId).then(user => {
		user.enrollments.push({courseId: data.courseId});
		return user.save().then((user, error) => {
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	})

	let isCourseUpdated = await Course.findById(data.courseId).then(course => {
		course.enrolless.push({userId: data.userId});
		return course.save().then((course, error) => {
			if(error){
				return false;
			}
			else {
				return true;
			}
		})
	})

	if(isUserUpdated && isCourseUpdated){
		return true;
	}
	else{
		return false;
	}
}