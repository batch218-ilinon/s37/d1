const User = require("../models/user.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js")

module.exports.checkEmailExist = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {
		if(result.length > 0){
			return true;
		}
		else{
			return false;
		}
	})	
};

module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		isAdmin: reqBody.isAdmin,
		password: bcrypt.hashSync(reqBody.password, 10),
		mobileNo: reqBody.mobileNo
	})

	return newUser.save().then((user, error) => {
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
}

module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null){
			return false;
		}
		else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result)};
			}
			else{
				return false;
			}
		}
	})
}


module.exports.getProfile = (reqBody) => {
	return User.find({_id: reqBody.id}).then(result => {
		if(result !== null){
			return result;
		}
		else{
			return false;
		}
	})	
};

//Part 5
module.exports.enrollCourse = (userId, reqBody) => {
	if(userId == reqBody.userId){
		return User.findByIdAndUpdate(userId, {$push: {enrollments: {courseId: reqBody.courseId}}})
				if(error){
					return false;
				}
				else{
					return true;
				}
				
		}
	else{
		let message = Promise.resolve('UserId failed to authenticate');
		return message.then((value) => {return value})
	}
}