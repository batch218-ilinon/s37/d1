const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const app = express();
const userRoute = require("./routes/userRoute.js");
const courseRoute = require("./routes/courseRoutes.js");

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use("/users", userRoute);
app.use("/courses", courseRoute);
mongoose.connect("mongodb+srv://admin:admin@batch218-coursebooking.zwvdy7o.mongodb.net/course-booking?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

mongoose.connection.once('open', () => console.log('Now connected to Edson-Mongo DB Atlas'));

app.listen(process.env.PORT || 4000, () => {console.log(`API is now online on port ${process.env.PORT || 4000}`)
});