const express = require("express");
const router = express.Router();

const courseController = require("../controllers/courseController.js");
const auth = require("../auth.js");


router.post("/create", (req,res) => {
	courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController))
})

router.get("/all", (req, res) => {
	courseController.getAllCourse().then(resultFromController => res.send(resultFromController))
})

router.get("/active", (req, res) => {
	courseController.getActiveCourses().then(resultFromController => res.send(resultFromController))
})

router.get("/:courseId", (req, res) => {
	courseController.getCourse(req.params.courseId).then(resultFromController => res.send(resultFromController))
})

//part 4
router.patch("/:courseId", auth.verify, (req, res) => {
	const newData = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	courseController.archiveCourse(req.params.courseId, newData).then(resultFromController => {
		res.send(resultFromController)
	})
})

router.patch("/:courseId/update", auth.verify, (req, res) => {
	const newData = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	courseController.updateCourse(req.params.courseId, newData).then(resultFromController => {
		res.send(resultFromController)
	})
})

// part 3
router.post("/", auth.verify, (req, res) => {
	const newData = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	courseController.createCourse(newData).then(resultFromController => {
		res.send(resultFromController)
	})
})



module.exports = router;