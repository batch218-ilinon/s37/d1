const express = require("express");
const router = express.Router();
const auth = require("../auth.js");

const userController = require("../controllers/userController.js");

router.post("/checkEmail", (req, res) => {
	userController.checkEmailExist(req.body).then(resultFromController => res.send(resultFromController))
});

router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
})

router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
})

router.post("/details", (req, res) => {
	userController.getProfile(req.body).then(resultFromController => res.send(resultFromController))
})

//part 5
router.post("/enroll", auth.verify, (req, res) => {
	const userId = auth.decode(req.headers.authorization).id
	userController.enrollCourse(userId, req.body).then(resultFromController => {
		res.send(resultFromController)
	})
})

module.exports = router;